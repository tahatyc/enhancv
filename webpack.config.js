const Path = require('path');
const Webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

NODE_ENV = 'development';

module.exports = {
    entry: './src/page/index.js',
    devtool: 'source-map',
    output: {
        path: Path.resolve(__dirname + '/dist'),
        filename: 'js/bundle.js',
        publicPath: '/'
    },
    stats: {
        // Adding this config prevents lots of "Child extract-text-webpack-plugin" messages
        children: false
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    use: ['style-loader', 'css-loader', 'resolve-url-loader'],
                    publicPath: '../'
                })
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader', 'resolve-url-loader'],
                    publicPath: '../'
                })
            },
            {
                test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                exclude: [/images/],
                loader: 'file-loader?name=fonts/[name].[ext]&limit=10000'
            },
            {
                test: /\.(jpg|jpeg|png|svg)(\?[a-z0-9]+)?$/,
                exclude: /node_modules/,
                loader: 'file-loader?name=images/[name].[ext]',
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/index.css'),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: '!!raw-loader!index.template.ejs'
        }),
        new CopyWebpackPlugin([
            { from: 'src/images', to: 'images' }
        ]),
        new UglifyJSPlugin({ sourceMap: true }),
        new OptimizeCssAssetsPlugin(),
        new Webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ],
    devServer: {
        contentBase: Path.join(__dirname, './dist'),
        historyApiFallback: true
    }
}

new Webpack.optimize.UglifyJsPlugin()