module.exports = {
    "extends": "airbnb",
    "rules": {
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "react/prefer-stateless-function": "off",
        "jsx-a11y/anchor-is-valid": "off",
        "class-methods-use-this": "off",
        "no-script-url": "off",
        "prefer-destructuring": "off",
        "max-len": ["error", 120, {"ignorePattern": "<path" }]
    },
    "globals": {
        "window": true,
        "document": true
    }
};