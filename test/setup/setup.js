import { configure } from 'enzyme';
import jsdom from 'jsdom';

function setUpDomEnvironment() {
	const { JSDOM } = jsdom;
	const dom = new JSDOM('<!DOCTYPE html><html><body><div id="app"></div></body></html>');
	const { window } = dom;

	global.window = window;
	global.document = window.document;
	global.navigator = {
		userAgent: 'node.js',
	};
	copyProps(window, global);
}

function copyProps(src, target) {
	const props = Object.getOwnPropertyNames(src)
		.filter(prop => typeof target[prop] === 'undefined')
		.map(prop => Object.getOwnPropertyDescriptor(src, prop));
	Object.defineProperties(target, props);
}

setUpDomEnvironment();