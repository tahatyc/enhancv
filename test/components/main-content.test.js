import chai, { expect } from 'chai';
import { mount, shallow, render } from 'enzyme';
import React from 'react';
import { CSSTransition } from 'react-transition-group';
import renderer from 'react-test-renderer';
import sinon from 'sinon';
import MainContent from '../../src/components/main-content/main-content';
import Header from '../../src/components/header/header';
import { STEPS_GUIDE, ANIMATION_TIMER } from '../../src/util/constants';

describe('Main Content', () => {
	let wrapper, header;

	beforeEach(() => {
		header = mount(<Header />);
		wrapper = mount(<MainContent />);
	});

	it('should hide the page loader', () => {
		const contentSection = document.getElementsByTagName('body')[0];
		wrapper.instance().onExitHide();

		expect(contentSection.classList.contains('loader-on')).to.equal(false);
		expect(contentSection.classList.contains('hidden-loader')).to.equal(true);
	});

	it('first textarea should be selected and to have class active-area', () => {
		const firstStepTextarea = wrapper.find('#' + STEPS_GUIDE[wrapper.state().step].id);

		expect(firstStepTextarea.node === document.activeElement).to.equal(true);
		expect(firstStepTextarea.parent().hasClass('active-area')).to.equal(true);
	});

	it('when the page loades the state fadeLoader should be false', () => {
		expect(wrapper.state().fadeLoader).to.equal(false);
	});

	/*
	it('when stopped tutorial, we should hide progress bar and sugestion text', () => {
		const tipsContent = wrapper.find('#text-tips');
		wrapper.find('#close-tutorial').simulate('click');

		expect(tipsContent.hasClass('hide-tips-container')).to.equal(true);
	});
	*/
});