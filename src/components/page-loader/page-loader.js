import React from 'react';
import { CSSTransition } from 'react-transition-group';
import BrandSVG from '../branding/brand-svg';

const PageLoader = (fadeLoader, onExitHide, removeElement) => {
  const fadeLoaderClass = fadeLoader ? 'fade-loader' : '';
  const removeElementClass = removeElement ? 'display-none' : '';

  return (
    <CSSTransition
      in={fadeLoader}
      timeout={1000}
      classNames="fade"
      onExited={onExitHide}
    >
      <div className={`page-loader ${fadeLoaderClass} ${removeElementClass}`}>
        <div className="svg-holder">
          {BrandSVG('loader-brand')}
        </div>
      </div>
    </CSSTransition>
  );
};

export default PageLoader;
