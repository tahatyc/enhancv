import React from 'react';

const BrandSVG = id => (
  <svg
    version="1.1"
    id={id}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    x="0px"
    y="0px"
    viewBox="0 0 44 23"
    xmlSpace="preserve"
  >
    <path className="st0" id="stem" stroke="#fff" fill="none" strokeDasharray="130" d="M22.1,14.8l-3.9,3.4C16.4,20,14,21,11.5,21c-2.5,0-4.9-1-6.7-2.8C3,16.4,2,14,2,11.5s1-4.9,2.8-6.7C6.7,2.8,9.3,1.8,12.1,2c2.4,0.1,4.6,1.3,6.4,3l16,15.9c0,0,0,0,0,0l7.1-13.3c0.4-0.7,0.5-1.5,0.4-2.4c-0.2-1.1-0.8-2-1.7-2.6c-1.5-1-3.4-0.8-4.7,0.3l-7,6.1" />
  </svg >
);

export default BrandSVG;
