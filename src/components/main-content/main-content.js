import React from 'react';
import propTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import PageLoader from '../page-loader/page-loader';
import UserOverview from '../main-content/user-overview';
import UserSkills from '../main-content/user-skills';
import UserContact from '../main-content/user-contact';
import { STEPS_GUIDE, ANIMATION_TIMER } from '../../util/constants';

export default class MainContent extends React.Component {
  constructor() {
    super();
    this.timeoutVar = '';
    this.state = {
      animation: true,
      fadeLoader: true,
      step: 0,
      doneStep: false,
      stopTutorial: false,
    };
  }

  componentDidMount() {
    // hide page loader when the component is loaded
    this.fadeLoader();
  }

  /*
  * Hide the loader overlay when the component is loaded
  */
  onExitHide() {
    const contentSection = document.getElementsByTagName('body')[0];
    contentSection.classList.remove('loader-on');
    contentSection.classList.add('hidden-loader');
  }

  /*
  * Change the state of the overlay loader of the page and focus on the first textarea so
  * that the user can populate it easely
  */
  fadeLoader() {
    this.setState({
      fadeLoader: false,
    });
    const firstStepTextarea = this[STEPS_GUIDE[this.state.step].inputSelector];
    firstStepTextarea.focus();
    firstStepTextarea.parentElement.classList.add('active-area');
  }

  /*
  * We are going to track when the user has stopped adding letters to the textarea and
  * we are going to send the user to the next step
  */
  handleKeyPress() {
    if (!this.state.stopTutorial) {
      const componentState = this;
      clearTimeout(this.timeoutVar);
      this.timeoutVar = setTimeout(() => {
        if (!componentState.state.doneStep) {
          componentState.setState({
            animation: !componentState.state.animation,
          });
          setTimeout(() => {
            componentState.setState({
              doneStep: true,
              animation: !componentState.state.animation,
            });
          }, ANIMATION_TIMER);
        }
      }, ANIMATION_TIMER);
    }
  }

  /*
  * This function will change the states for the next step and will focus on the next textarea
  * acording to the step in the tutorial
  */
  nextStep() {
    const componentState = this;

    setTimeout(() => {
      componentState.setState({
        animation: !componentState.state.animation,
      });
      setTimeout(() => {
        componentState.setState({
          step: componentState.state.step + 1,
          doneStep: false,
          animation: !componentState.state.animation,
        });
        const stepsLen = Object.keys(STEPS_GUIDE).length;
        const calculateProgress = ((componentState.state.step) * 100) / (stepsLen - 1);
        const prevStepTextarea = this[STEPS_GUIDE[componentState.state.step - 1].inputSelector];
        const nextStepTextarea = this[STEPS_GUIDE[componentState.state.step].inputSelector];

        prevStepTextarea.parentElement.classList.remove('active-area');

        document.getElementById('progress-bar').style.width = `${calculateProgress}%`;

        // don't focus next element if we are on the last step
        if (nextStepTextarea) {
          nextStepTextarea.focus();
          nextStepTextarea.parentElement.classList.add('active-area');
        } else {
          componentState.closeBtn.classList.remove('hide');
        }
      }, ANIMATION_TIMER);
    }, ANIMATION_TIMER);
  }

  /*
  * When we are at the and of the tutorial we can close it and continue with modifying our CV
  * in normal mode
  */
  stopTutorial() {
    this.tipTextContainer.classList.add('hide-tips-container');
    this.resumeOvelay.style.display = 'none';
    this.props.progressRefProp.classList.add('hide-progress');

    this.setState({
      stopTutorial: !this.state.stopTutorial,
    });
  }

  /*
  * This function will render the current text of the step that the user is on at the moment
  */
  renderCurrentTipText() {
    const link = STEPS_GUIDE[this.state.step].link;

    return (
      <div>
        {!this.state.doneStep && <p>{STEPS_GUIDE[this.state.step].stepLabel}</p>}
        {!this.state.doneStep &&
          <p className="sub-title">
            {STEPS_GUIDE[this.state.step].stepSubLabel}
            {STEPS_GUIDE[this.state.step].link &&
              <a href={link}>{STEPS_GUIDE[this.state.step].linkLabel}</a>}
          </p>}
      </div>
    );
  }

  /*
  * This helper function will render the tips for the steps and the next button for the next
  * task for the user
  */
  renderFinishedTipText() {
    return (
      <div>
        <p className="finished-step">{STEPS_GUIDE[this.state.step].done}</p>
        {STEPS_GUIDE[this.state.step].subDoneText &&
          <p className="sub-title">
            {STEPS_GUIDE[this.state.step].subDoneText}
          </p>
        }
        <a className="btn btn-secondary next-step" onClick={() => this.nextStep()} href="javascript:void(0)">
          Next
        </a>
      </div>
    );
  }

  render() {
    return (
      <div className="content-section" id="content-section">
        {PageLoader(this.state.fadeLoader, this.onExitHide)}
        <div
          className="text-tips"
          id="text-tips"
          ref={(tipTextContainer) => { this.tipTextContainer = tipTextContainer; }}
        >
          <CSSTransition
            in={this.state.animation}
            timeout={ANIMATION_TIMER}
            classNames="fade"
          >
            <div className="content">
              {!this.state.doneStep && this.renderCurrentTipText()}
              {this.state.doneStep && this.renderFinishedTipText()}
            </div>
          </CSSTransition>
          <a
            ref={(closeBtn) => { this.closeBtn = closeBtn; }}
            onClick={() => { this.stopTutorial(); }}
            className="close-tutorial hide"
            id="close-tutorial"
            href="javascript:void(0)"
          >
            <i className="fa fa-times" />
          </a>
        </div>
        <div className="resume">
          <div className="background-modification" />
          <div className="active-writing" ref={(resumeOvelay) => { this.resumeOvelay = resumeOvelay; }} />
          <div className="row">
            <div className="col-md-7">
              <div className="user-name-container">
                <textarea
                  ref={(userName) => { this.userName = userName; }}
                  id="user-name"
                  onKeyUp={() => this.handleKeyPress()}
                  onKeyDown={() => clearTimeout(this.timeoutVar)}
                  name="user-name"
                  maxLength="100"
                  spellCheck="false"
                  autoComplete="off"
                  rows="1"
                  placeholder="Your Name"
                />
              </div>
              <div className="user-role-container">
                <textarea
                  ref={(userRole) => { this.userRole = userRole; }}
                  id="user-role"
                  onKeyUp={() => this.handleKeyPress()}
                  onKeyDown={() => clearTimeout(this.timeoutVar)}
                  name="user-role"
                  maxLength="100"
                  spellCheck="false"
                  autoComplete="off"
                  rows="1"
                  placeholder="Your next desired role?"
                />
              </div>
              {UserContact()}
            </div>
            <div className="col-md-5">
              <div className="avatar" />
            </div>
          </div>
          <div className="row">
            <div className="col-md-7">
              {UserOverview()}
            </div>
            <div className="col-md-5">
              {UserSkills()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MainContent.propTypes = {
  progressRefProp: propTypes.func,
};

MainContent.defaultProps = {
  progressRefProp: () => {},
};
