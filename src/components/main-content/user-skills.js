import React from 'react';

const UserSkills = () => (
  <div>
    <div className="content">
      <h2>Technologies</h2>
      <h3>Frontend</h3>
      <ul>
        <li>React</li>
        <li>Angular</li>
        <li>ES6</li>
        <li>Grunt</li>
        <li>Webpack</li>
        <li>Less</li>
        <li>Sass</li>
        <li>Javascript</li>
        <li>jQuery</li>
      </ul>
    </div>
    <div className="content">
      <h3>Backend and CV</h3>
      <ul>
        <li>Nodejs</li>
        <li>PHP</li>
        <li>Git</li>
      </ul>
    </div>
  </div>
);

export default UserSkills;
