import React from 'react';

const UserContact = () => (
  <div className="user-info">
    <div className="icon-container">
      <i className="fa fa-phone" />
      <p>0898604787</p>
    </div>
    <div className="icon-container">
      <i className="fa fa-at" />
      <p>jorry_pet@abv.bg</p>
    </div>
    <div className="icon-container">
      <i className="fa fa-map-marker" />
      <p>Sofia, Bulgaria</p>
    </div>
  </div>
);

export default UserContact;
