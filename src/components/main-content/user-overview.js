import React from 'react';

const UserOverview = () => (
  <div>
    <div className="content">
      <h2>Summary</h2>
      <p>
        Highly motivated software developer, eager to learn new technologies and improve
        in all aspects of Web Development sector, particular interest in developing
        responsive, standards-compliant web applications. Highly reliable work
        ethics and customer-focused manner of work.
      </p>
    </div>
    <div className="content">
      <h2>Most proud of</h2>
      <p>
        Highly motivated software developer, eager to learn new technologies and improve
        in all aspects of Web Development sector, particular interest in developing
        responsive, standards-compliant web applications. Highly reliable work
        ethics and customer-focused manner of work.
      </p>
    </div>
  </div>
);

export default UserOverview;

