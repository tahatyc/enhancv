import React from 'react';
import propTypes from 'prop-types';
import BrandSVG from '../branding/brand-svg';

export default class Header extends React.Component {
  render() {
    return (
      <header className="navigation-header">
        <div className="container-fluid">
          <div className="row header-holder">
            <div className="col-md-4">
              <a className="branding" href="/">
                {BrandSVG('header-brand')}
              </a>
            </div>
            <div className="col-md-4">
              <div ref={this.props.progressRef} id="progress-holder">
                <div className="progress-label">Progress</div>
                <div className="progress">
                  <div
                    style={{ width: '0%' }}
                    id="progress-bar"
                    className="progress-bar"
                    role="progressbar"
                    aria-valuenow="0"
                    aria-valuemin="0"
                    aria-valuemax="100"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-4 text-right">
              <a className="btn btn-secondary" href="#">Sign in</a>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  progressRef: propTypes.func,
};

Header.defaultProps = {
  progressRef: null,
};
