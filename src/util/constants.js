export const STEPS_GUIDE = {
  0: {
    stepLabel: '5 quick steps to Enhancv. First, type your name.',
    done: "Well Done! Select 'Next' to continue.",
    inputSelector: 'userName',
    id: 'user-name',
  },
  1: {
    stepLabel: 'Now type the role you are apllying for',
    stepSubLabel: 'E.g. Senior Web Developer, Product Marketing Manager, CEO, Director of finance, Retail Manager',
    done: 'Enter a specific role. It helps us presonlize your resume template to it.',
    subDoneText: 'E.g. Senior Web Developer, Product Marketing Manager, CEO, Director of finance, Retail Manager',
    inputSelector: 'userRole',
    id: 'user-role',
  },
  2: {
    stepLabel: 'Great your resume is now tailored for a {SENIORITY}, position in {INDUSTRY}',
    stepSubLabel: 'This is not the role you are apllying for?',
    linkLabel: 'Change Next Desired Role',
    link: '#',
  },
};

export const ANIMATION_TIMER = 300;
