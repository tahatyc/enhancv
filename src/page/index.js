import React from 'react';
import ReactDOM from 'react-dom';

import Header from '../components/header/header';
import MainContent from '../components/main-content/main-content';
import '../scss/main.scss';


class App extends React.Component {
  constructor() {
    super();

    this.state = {
      progressRef: null,
    };
  }

  componentDidMount() {
    this.headerHref();
  }

  headerHref() {
    this.setState({
      progressRef: this.progressRef,
    });
  }

  render() {
    return (
      <div className="app-holder">
        <Header progressRef={(c) => { this.progressRef = c; }} />
        <MainContent progressRefProp={this.state.progressRef} />
      </div>
    );
  }
}

const app = document.getElementById('app');
ReactDOM.render(<App />, app);
